----------------------------------
-- (1) a. covid cases
----------------------------------
with monthly_cases as (

    select date_trunc(date, month) as period
        , sum(new_confirmed)       as covid_cases
    from `assignment_environment.covid_cases_assignment`
    where lower(country_code) = "us"
    group by date

)

-- you can extract year and month from this date by using:
--     extract(year from period) as year
--     extract(month from period) as month 
--          to just get the year and month in separate columns.

select period                     -- standardized date
     , extract(year from period)  as year
     , extract(month from period) as month  
  from monthly_cases 
 order by covid_cases desc
 limit 1

----------------------------------
 -- (1) b. advertising metrics
----------------------------------

with account_data as (
    
    select row_number() over ( partition by advertiser_id
                                   order by date_updated desc ) as row_num
         , date_updated
         , advertiser_id, country_code, category, billing_status
      from `assignment_environment.account_data`

)

, latest_account_data as (

    select date_updated, advertiser_id, country_code, category, billing_status
    from account_data
    where row_num = 1

) 

select date_trunc(pd.date, month) as period          --0
     , pd.country_code, pd.advertiser_id
     , ad.category
     , sum(pd.sales)                        as sales --1
     , sum(pd.spend)                        as spend --2
     , sum(pd.clicks) / sum(pd.impressions) as ctr   --3
  from `assignment_environment.performance_data` pd
  left join latest_account_data ad
    on pd.advertiser_id = ad.advertiser_id
 group by pd.date
     , pd.country_code, pd.advertiser_id
     , ad.category

----------------------------------
-- (2) - combination of 3 tables
----------------------------------


with account_data as (
    
    select row_number() over ( partition by advertiser_id
                                   order by date_updated desc ) as row_num
         , date_updated
         , advertiser_id, country_code, category, billing_status
      from `assignment_environment.account_data`

)

, latest_account_data as (

    select date_updated, advertiser_id, country_code, category, billing_status
      from account_data
     where row_num = 1

) 

, monthly_cases as (

    select date_trunc(date, month) as period
         , country_code
         , sum(new_confirmed)       as covid_cases
      from `assignment_environment.covid_cases_assignment`
     group by date, country_code

)

, sales_data as (

    select date_trunc(pd.date, month) as period
         , pd.country_code, pd.advertiser_id
         , ad.category
         , sum(pd.sales)                        as sales
         , sum(pd.spend)                        as spend 
         , sum(pd.impressions)                  as impressions
         , sum(pd.clicks)                       as clicks
         , sum(pd.conversions)                  as conversions
        
        -- better calculated on data studio for dynamic calculation
        --  , sum(pd.clicks) / sum(pd.impressions) as ctr
      
      from `assignment_environment.performance_data` pd
      left join latest_account_data ad
        on pd.advertiser_id = ad.advertiser_id
     where lower(ad.billing_status) = "active"
           and lower(ad.category) in ("jewelry", "health & supplements")
     group by pd.date
         , pd.country_code, pd.advertiser_id
         , ad.category

)

-- this will combine the 3 tables at the standardized monthly level
-- to understand covid cases in a given country and advertisement metrics
-- syncronized over the same time period without the advertiser_id.

select sd.period, sd.country_code, sd.category
     , sum(sd.sales)                        as sales
     , sum(sd.spend)                        as spend
     , sum(sd.impressions)                  as impressions
     , sum(sd.clicks)                       as clicks
     , sum(sd.conversions)                  as conversions
     , sum(mc.covid_cases)                  as covid_cases
  from sales_data sd
 inner join monthly_cases mc
    on sd.period = mc.period
       and sd.country_code = mc.country_code
 group by sd.period, sd.country_code, sd.category
