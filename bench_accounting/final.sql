with monthly_invoice as (

    select date_trunc(i.invoice_date, month)                          as invoice_month
         , i.invoice
         , i.stripe_customer
         , i.product
         , ifnull(d.discount_amount, 0)                               as discount_amount

      from `bench_accounting.invoices` i
      left join `bench_accounting.discounts` d
        on lower(i.invoice) = lower(d.invoice)

)

, aggregated_metrics as (

    select mi.invoice_month
         , mi.stripe_customer
         , mi.product
         , count(distinct mi.invoice)                                 as total_service_units
         , sum(mi.discount_amount)                                    as total_discount
      from monthly_invoice mi
     group by mi.invoice_month, mi.stripe_customer, mi.product

)

, final as (

    select am.invoice_month, am.stripe_customer, am.product
         , am.total_service_units, am.total_discount

         , (am.total_service_units * p.gross_mrr)                     as total_gross_revenue
         , (am.total_service_units * p.gross_mrr) - am.total_discount as total_net_revenue
      from aggregated_metrics am
      left join `bench_accounting.products` p
        on am.product = p.product

)

select *
  from final;
  